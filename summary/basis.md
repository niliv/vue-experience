# 基础

## eslint

vue 项目常见 eslint 配置
http://www.a4z.cn/fe/2018/11/16/vscode-eslint-prettier-cr-error/

## 调试

### vscode 调试

下载 Debugger for Chrome
vscode 添加配置
···bash
{
// 使用 IntelliSense 了解相关属性。
// 悬停以查看现有属性的描述。
// 欲了解更多信息，请访问: https://go.microsoft.com/fwlink/?linkid=830387
"version": "0.2.0",
"configurations": [{
"type": "chrome",
"request": "launch",
"name": "vue chrome",
"url": "http://localhost:8080",
"webRoot": "${workspaceFolder}/src",
"breakOnLoad": true,
"sourceMapPathOverrides": {
"webpack:///src/*": "${webRoot}/*",
"webpack:///./*": "${webRoot}/*"
}
}]
}

````
打断点，瓢虫运行

### devtool

chrome插件devtool安装
代码debugger
运行会自动中断
console中可以this.msg看到变量


## Vue 实例

阻止响应系统

​```javascript
var obj = {
  foo: 'bar'
}

Object.freeze(obj)

new Vue({
  el: '#app',
  data: obj
})
````

## 模板

支持数据和 javascript 表达式，判断语句使用三元表达式

```javascript
<div v-bind:id="'list-' + id"></div>
```

## 指令

指令特性的值预期是**单个 JavaScript 表达式**

从 2.6.0 开始，可以用方括号括起来的 JavaScript 表达式作为一个指令的参数

动态参数表达式有一些语法约束，因为某些字符，例如空格和引号，放在 HTML 特性名里是无效的。同样，在 DOM 中使用模板时你需要回避大写键名。

```javascript
<a v-bind:[attributeName]="url"> ... </a>
```

### 嵌套循环

```javascript
<!-- 嵌套循环 -->
    <ul>
      <li v-for="(item, key) in list1" :key="key">
        {{ item.cate }}
        <ol>
          <li v-for="(news, key) in item.list" :key="key">
            {{ news.title }}
          </li>
        </ol>
      </li>
    </ul>
```

### 修饰符

修饰符 (modifier) 是以半角句号 `.` 指明的特殊后缀，用于指出一个指令应该以特殊方式绑定。例如，`.prevent` 修饰符告诉 `v-on` 指令对于触发的事件调用 `event.preventDefault()`

`<form v-on:submit.prevent="onSubmit">...</form>`

## API

### computed

```javascript
var vm = new Vue({
  el: '#demo',
  data: {
    firstName: 'Foo',
    lastName: 'Bar'
  },
  computed: {
    fullName: function() {
      return this.firstName + ' ' + this.lastName
    }
  }
})
```

```javascript
<div id="demo">
		姓：<input type="text" placeholder="firstName" v-model="firstName"><br>
		名：<input type="text" placeholder="lastName" v-model="lastName"><br>
		姓名1(单向):<input type="text" placeholder="FullName1" v-model="fullName1"><br>
		姓名2(双向):<input type="text" placeholder="FullName2" v-model="fullName2"><br>
</div>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
    const demo = new Vue({
		el : '#demo',
		data : {
			firstName : 'A',
			lastName : 'B',
			fullName2 : 'A B'
		},
		computed : {//计算属性相当于data里的属性
			//什么时候执行：初始化显示/ 相关的data属性发生变化
			fullName1(){//计算属性中的get方法，方法的返回值就是属性值
				return this.firstName + ' ' + this.lastName
			},
 
			fullName3 : {
				get(){//回调函数 当需要读取当前属性值是执行，根据相关数据计算并返回当前属性的值
					return this.firstName + ' ' + this.lastName
				},
				set(val){//监视当前属性值的变化，当属性值发生变化时执行，更新相关的属性数据
					//val就是fullName3的最新属性值
					console.log(val)
					const names = val.split(' ');
					console.log(names)
					this.firstName = names[0];
					this.lastName = names[1];
				}
			}
		}
 
	})
```



### watch

```javascript
<div id="watch-example">
  <p>
    Ask a yes/no question:
    <input v-model="question">
  </p>
  <p>{{ answer }}</p>
</div>
```

```javascript
<!-- 因为 AJAX 库和通用工具的生态已经相当丰富，Vue 核心代码没有重复 -->
<!-- 提供这些功能以保持精简。这也可以让你自由选择自己更熟悉的工具。 -->
<script src="https://cdn.jsdelivr.net/npm/axios@0.12.0/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>
<script>
var watchExampleVM = new Vue({
  el: '#watch-example',
  data: {
    question: '',
    answer: 'I cannot give you an answer until you ask a question!'
  },
  watch: {
    // 如果 `question` 发生改变，这个函数就会运行
    question: function (newQuestion, oldQuestion) {
      this.answer = 'Waiting for you to stop typing...'
      this.debouncedGetAnswer()
    }
  },
  created: function () {
    // `_.debounce` 是一个通过 Lodash 限制操作频率的函数。
    // 在这个例子中，我们希望限制访问 yesno.wtf/api 的频率
    // AJAX 请求直到用户输入完毕才会发出。想要了解更多关于
    // `_.debounce` 函数 (及其近亲 `_.throttle`) 的知识，
    // 请参考：https://lodash.com/docs#debounce
    this.debouncedGetAnswer = _.debounce(this.getAnswer, 500)
  },
  methods: {
    getAnswer: function () {
      if (this.question.indexOf('?') === -1) {
        this.answer = 'Questions usually contain a question mark. ;-)'
        return
      }
      this.answer = 'Thinking...'
      var vm = this
      axios.get('https://yesno.wtf/api')
        .then(function (response) {
          vm.answer = _.capitalize(response.data.answer)
        })
        .catch(function (error) {
          vm.answer = 'Error! Could not reach the API. ' + error
        })
    }
  }
})
</script>
```

watch 对象深度监控

watch 获取 dom 元素

```javascript
<p ref='watchO'></p>
<p ref='watchN'></p>

// 监视单个数据的变化
watch: {
    obj: {
        deep: true,
            handler(n,o){
            console.log(o,n)
        }
    },
        mySubText: function (n,o) {
            this.$refs.watchO.innerText = o;
            this.$refs.watchN.innerText = n;
        }
},
```

### 属性

```javascript
<!-- 绑定属性 -->
    <div :title="msg">鼠标上去看一下</div>
    <img :src="img_url" alt="no" />
    <div v-html="h"></div>
```

## Class && Style

```html
<div
  class="static"
  v-bind:class="{ active: isActive, 'text-danger': hasError }"
></div>
```

```javascript
data: {
  isActive: true,
  hasError: false
}
```

`<div class="static active"></div>`

```javascript
<div v-bind:class="classObject"></div>
```

```javascript
data: {
  isActive: true,
  error: null
},
computed: {
  classObject: function () {
    return {
      active: this.isActive && !this.error,
      'text-danger': this.error && this.error.type === 'fatal'
    }
  }
}
```

```javascript
<div v-bind:class="[activeClass, errorClass]"></div>
```

```javascript
data: {
  activeClass: 'active',
  errorClass: 'text-danger'
}
```

```javascript
<div v-bind:class="[isActive ? activeClass : '', errorClass]"></div>
```

```javascript
<div v-bind:style="styleObject"></div>
```

```javascript
data: {
  styleObject: {
    color: 'red',
    fontSize: '13px'
  }
}
```

```javascript
<!-- 绑定样式 -->
    <!-- class -->
    <div :class="{ red: flag }">
      我是div
    </div>
    <div :class="{ red: flag, blue: !flag }">
      我是另一个div
    </div>
    <ul>
      <li
        v-for="(item, key) in list"
        :key="key"
        :class="{ red: key == 0, blue: key == 1 }"
      >
        {{ item }}
      </li>
    </ul>
    <!-- style -->
    <div class="box" :style="boxStyle">
      我是div
    </div>
    <br /><br />

     flag: false,
      boxStyle: {
        width: 500 + 'px'
      },
```

## ref

```javascript
<!-- ref -->
    <input type="text" ref="userinfo" />
    <button @click="getDom">获取dom</button>
    <div ref="box">123</div>
    <button @click="setDom">改变dom</button>
    <br /><br />
```

```javascript
getDom () {
    console.log(this.$refs.userinfo.value)
},
    setDom () {
        this.$refs.box.style.background = 'red';
    },
```

## 事件对象

```javascript
<!-- 事件对象 -->
    <button data-id="123" @click="eventFn($event)">事件</button>

eventFn (e) {
    console.log(e.srcElement.dataset.id)
}
```

## v-if && v-show

### v-if

用 key 管理可复用的元素

```javascript
<template v-if="loginType === 'username'">
  <label>Username</label>
  <input placeholder="Enter your username">
</template>
<template v-else>
  <label>Email</label>
  <input placeholder="Enter your email address">
</template>
```

那么在上面的代码中切换 `loginType` 将不会清除用户已经输入的内容。因为两个模板使用了相同的元素，`<input>` 不会被替换掉——仅仅是替换了它的 `placeholder`

```javascript
<template v-if="loginType === 'username'">
  <label>Username</label>
  <input placeholder="Enter your username" key="username-input">
</template>
<template v-else>
  <label>Email</label>
  <input placeholder="Enter your email address" key="email-input">
</template>
```

这样也不总是符合实际需求，所以 Vue 为你提供了一种方式来表达“这两个元素是完全独立的，不要复用它们”。只需添加一个具有唯一值的 `key` 属性即可

一般来说，`v-if` 有更高的切换开销，而 `v-show` 有更高的初始渲染开销。因此，如果需要非常频繁地切换，则使用 `v-show` 较好；如果在运行时条件很少改变，则使用 `v-if` 较好。

## 变更检测

### 数组

由于 JavaScript 的限制，Vue **不能**检测以下数组的变动：

1. 当你利用索引直接设置一个数组项时，例如：`vm.items[indexOfItem] = newValue`
2. 当你修改数组的长度时，例如：`vm.items.length = newLength`

```javascript
var vm = new Vue({
  data: {
    items: ['a', 'b', 'c']
  }
})
vm.items[1] = 'x' // 不是响应性的
vm.items.length = 2 // 不是响应性的
```

解决

```javascript
// Vue.set
Vue.set(vm.items, indexOfItem, newValue)

vm.items.splice(newLength)
```

### 对象

还是由于 JavaScript 的限制，**Vue 不能检测对象属性的添加或删除**

```javascript
var vm = new Vue({
  data: {
    a: 1
  }
})
// `vm.a` 现在是响应式的

vm.b = 2
// `vm.b` 不是响应式的
```

```javascript
var vm = new Vue({
  data: {
    userProfile: {
      name: 'Anika'
    }
  }
})
```

你可以添加一个新的 `age` 属性到嵌套的 `userProfile` 对象

`Vue.set(vm.userProfile, 'age', 27)`

你可能需要为已有对象赋值多个新属性

```javascript
vm.userProfile = Object.assign({}, vm.userProfile, {
  age: 27,
  favoriteColor: 'Vue Green'
})
```

## 事件

[事件修饰符](https://cn.vuejs.org/v2/guide/events.html)

## form

### 复选框

```javascript
<div id='example-3'>
  <input type="checkbox" id="jack" value="Jack" v-model="checkedNames">
  <label for="jack">Jack</label>
  <input type="checkbox" id="john" value="John" v-model="checkedNames">
  <label for="john">John</label>
  <input type="checkbox" id="mike" value="Mike" v-model="checkedNames">
  <label for="mike">Mike</label>
  <br>
  <span>Checked names: {{ checkedNames }}</span>
</div>
```

```javascript
new Vue({
  el: '#example-3',
  data: {
    checkedNames: []
  }
})
```

### 下拉

```javascript
<select v-model="selected">
    <option disabled value="">请选择</option>
    <option>A</option>
    <option>B</option>
    <option>C</option>
  </select>
  <span>Selected: {{ selected }}</span>
```

### 修饰符

```javascript
<!-- 在“change”时而非“input”时更新 -->
<input v-model.lazy="msg" >

<input v-model.number="age" type="number">

<input v-model.trim="msg">
```

## 生命周期

```javascript
beforeCreate() {
	console.log("beforeCreate data " + this.mySubText) // 组件创建之前，undefined
},
created() {
	console.log("created data " + this.mySubText) // 组件创建之后，可以操作数据，hello com  可以掉ajax
	console.log("created ref " + this.$refs.watchP)
},
beforeMount() {
	console.log("beforeMount data " + this.mySubText) // vue起作用，装在到dom之前
	console.log("beforeMount ref " + this.$refs.watchP)
},
mounted() {
	console.log('mounted') // vue起作用，装在到dom之后  //也可以在这里调ajax
	console.log("mounted ref " + this.$refs.watchP)
	this.isExit = true
	//isExit更新需要时间，所以这里要用回调，也可以在update里获取
	this.$nextTick(function () {
		this.$refs.input.focus();
	})
},
beforeUpdate() {
	console.log('beforeUpdate') // 更新前
},
updated() {
	console.log('updated') // 更新后
},
beforeDestroy() {
	console.log('beforeDestroy') //
},
destroyed() {
	console.log('destroyed') //
},
activated() {
	console.log('activated') // 避免频繁销毁创建，被keep-alive包含
},
deactivated() {
	console.log('deactivated') // 避免频繁销毁创建，被keep-alive包含
}
```

# 组件

## 组件注册

### 全局组件

`Vue.component`，template、data、methods、watch、computed、filters等

data必须是一个函数

```javascript
Vue.component(
    'my-global-lib', {
        template: `
<div>
v-model: <input type='text' v-model='myText'> {{myText}}
</div>
`,
        data: function () {
            return {
                myText: '阿大使多'
            }
        },
        methods: {
            changeShow() {
                this.isShow = !this.isShow;
            }
        },
        filters: {
            reverse: function (datastr, lang) {
                return lang + ' ' + datastr.split('').reverse().join('');
            }
        },
        watch:{
            myText:function(n,o){
                n=n+'123';
            }
        },
        computed:{
            result:function(){
                return (+this.n1)+(+this.n2)+(+this.n3);
            }
        }
    }
);
```

### 局部组件

```javascript
var ComponentA = { /* ... */ }
var ComponentB = { /* ... */ }
var ComponentC = { /* ... */ }
```

```javascript
new Vue({
  el: '#app',
  components: {
    'component-a': ComponentA,
    'component-b': ComponentB
  }
})
```





## 组件传值

### 父传子

#### props

HTML 中的特性名是大小写不敏感的，所以浏览器会把所有大写字符解释为小写字符。这意味着当你使用 DOM 中的模板时，camelCase (驼峰命名法) 的 prop 名需要使用其等价的 kebab-case (短横线分隔命名) 命名

```javascript
Vue.component('blog-post', {
  // 在 JavaScript 中是 camelCase 的
  props: ['postTitle'],
  template: '<h3>{{ postTitle }}</h3>'
})

<!-- 在 HTML 中是 kebab-case 的 -->
<blog-post post-title="hello!"></blog-post>
```



```javascript
Vue.component('blog-post', {
  props: ['title'],
  template: '<h3>{{ title }}</h3>'
})

<blog-post title="My journey with Vue"></blog-post>
```

```javascript
Vue.component('blog-post', {
  props: ['post'],
  template: `
    <div class="blog-post">
      <h3>{{ post.title }}</h3>
      <div v-html="post.content"></div>
    </div>
  `
})

<blog-post
  v-for="post in posts"
  v-bind:key="post.id"
  v-bind:post="post"
></blog-post>
```

每次父级组件发生更新时，子组件中所有的 prop 都将会刷新为最新的值。这意味着你**不**应该在一个子组件内部改变 prop。如果你这样做了，Vue 会在浏览器的控制台中发出警告

1. **这个 prop 用来传递一个初始值；这个子组件接下来希望将其作为一个本地的 prop 数据来使用。**在这种情况下，最好定义一个本地的 data 属性并将这个 prop 用作其初始值：

```javascript
props: ['initialCounter'],
data: function () {
  return {
    counter: this.initialCounter
  }
}
```

2. **这个 prop 以一种原始的值传入且需要进行转换。**在这种情况下，最好使用这个 prop 的值来定义一个计算属性：

```javascript
props: ['size'],
computed: {
  normalizedSize: function () {
    return this.size.trim().toLowerCase()
  }
}
```

**验证**

```javascript
Vue.component('my-component', {
  props: {
    // 基础的类型检查 (`null` 和 `undefined` 会通过任何类型验证)
    propA: Number,
    // 多个可能的类型
    propB: [String, Number],
    // 必填的字符串
    propC: {
      type: String,
      required: true
    },
    // 带有默认值的数字
    propD: {
      type: Number,
      default: 100
    },
    // 带有默认值的对象
    propE: {
      type: Object,
      // 对象或数组默认值必须从一个工厂函数获取
      default: function () {
        return { message: 'hello' }
      }
    },
    // 自定义验证函数
    propF: {
      validator: function (value) {
        // 这个值必须匹配下列字符串中的一个
        return ['success', 'warning', 'danger'].indexOf(value) !== -1
      }
    }
  }
})
```

```javascript
<template>
  <div>
    <h2>{{ msg + "---" + title }}</h2>
    <button @click="run()">父组件方法</button>
  </div>
</template>
<script>
export default {
  data () {
    return {
      msg: '我是头部'
    }

  },
  props: ['title', 'run']
}
</script>
//父
<v-header :title="title" :run="run"></v-header>
 methods: {
    run () {
      console.log(this.msg)
    }
  },
```



### 子传父

`emit`

```javascript
<button v-on:click="$emit('enlarge-text', 0.1)">
  Enlarge text
</button>

<blog-post
  ...
  v-on:enlarge-text="onEnlargeText"
></blog-post>
methods: {
  onEnlargeText: function (enlargeAmount) {
    this.postFontSize += enlargeAmount
  }
}
```

### 父主动获取子

```javascript
<v-header ref="header"></v-header>
<button @click="getHeader">获取header数据</button>
<button @click="getHeaderM">获取header方法</button>

getHeader () {
    console.log(this.$refs.header.msg)
},
getHeaderM () {
   console.log(this.$refs.header.run(0))
}
```

### 子主动获取父

```javascript
<button @click="getParentData">获取父组件数据</button>
<button @click="getParentM">获取父组件方法</button>

getParentData () {
    console.log(this.$parent.msg)
},
getParentM () {
   console.log(this.$parent.run())
}
```

## 组件传值2

### 父子传值

props $emit $children $parent $ref

```vue
<template>
  <div>
    <span>我是父组件</span> <span>{{ childhamsg }}</span>
    <child
      childmsg="i am from parent"
      @childEvent="childEvent"
      ref="childcom"
    ></child>
    {{ fromChild }}
  </div>
</template>

<script>
import child from '@/views/child.vue'
export default {
  components: {
    child
  },
  mounted() {
    console.log(this.$children[0].child_msg)
    console.log(this.$refs.childcom.child_msg)
  },
  data() {
    return {
      fromChild: ''
    }
  },
  computed: {
    /* childhamsg() {
      return this.$children[0].child_msg
    } */
  },
  methods: {
    childEvent(value) {
      this.fromChild = value
    }
  }
}
</script>

<style lang="scss" scoped></style>

```

```vue
<template>
  <div>
    我是子组件
    {{ childmsg }}
    <button @click="toParent">去父组件</button>
  </div>
</template>

<script>
export default {
  props: {
    childmsg: {
      type: String,
      default: ''
    }
  },
  data() {
    return {
      child_msg: 'look i am child'
    }
  },
  methods: {
    toParent() {
      this.$emit('childEvent', 'i am from child')
    }
  }
}
</script>

<style lang="scss" scoped></style>

```

### 非父子

#### bus

```vue
import Vue from 'vue'
export default new Vue;
// 在需要传递消息的地⽅方引⼊入
import bus from './bus.js'
// 传递消息
bus.$emit('msg', val)
// 接受消息
bus.$emit('msg', val => {
console.log(val)
})
```

#### $attrs $listeners

```vue
<template>
  <div id="app">
    <button @click="passMsg">传child</button>
    <router-view />
    <mparent :msga="msga" :msgb="msgb" @childEvent="childEvent"></mparent>
  </div>
</template>
<script>
import bus from './util/bus'
import mparent from './views/parent'
export default {
  components: {
    mparent
  },
  data() {
    return {
      msga: 'a',
      msgb: 'b'
    }
  },

  methods: {
    passMsg() {
      bus.$emit('tochild', 'i am from app')
    },
    childEvent(v) {
      console.log(v)
    }
  }
}
</script>
<style lang="scss"></style>

```

```vue
<template>
  <div>
    <span>我是父组件</span>
    <child
      childmsg="i am from parent"
      @childEvent="childEvent"
      ref="childcom"
      v-bind="$attrs"
      v-on="$listeners"
    ></child>
    {{ fromChild }}
  </div>
</template>

<script>
import child from '@/views/child.vue'
export default {
  components: {
    child
  },
  mounted() {
    console.log(this.$children[0].child_msg)
    console.log(this.$refs.childcom.child_msg)
    console.log('attrs', this.$attrs)
  },
  data() {
    return {
      fromChild: ''
    }
  },
  computed: {
    /* childhamsg() {
      return this.$children[0].child_msg
    } */
  },
  methods: {
    childEvent(value) {
      this.fromChild = value
    }
  }
}
</script>

<style lang="scss" scoped></style>

```

```vue
<template>
  <div>
    我是子组件
    {{ childmsg }}
    <button @click="toParent">去父组件</button>
  </div>
</template>

<script>
import bus from '../util/bus'
export default {
  props: {
    childmsg: {
      type: String,
      default: ''
    }
  },
  data() {
    return {
      child_msg: 'look i am child'
    }
  },
  mounted() {
    bus.$on('tochild', v => {
      this.childmsg = v
    })
    console.log('attrs', this.$attrs)
  },
  methods: {
    toParent() {
      this.$emit('childEvent', 'i am from child')
    }
  }
}
</script>

<style lang="scss" scoped></style>

```





## slot

```javascript
<alert-box>
  Something bad happened.
</alert-box>

Vue.component('alert-box', {
  template: `
    <div class="demo-alert-box">
      <strong>Error!</strong>
      <slot></slot>
    </div>
  `
})
```

```javascript
<div id="todo-list-example">
  <form v-on:submit.prevent="addNewTodo">
    <label for="new-todo">Add a todo</label>
    <input
      v-model="newTodoText"
      id="new-todo"
      placeholder="E.g. Feed the cat"
    >
    <button>Add</button>
  </form>
  <ul>
    <li
      is="todo-item"
      v-for="(todo, index) in todos"
      v-bind:key="todo.id"
      v-bind:title="todo.title"
      v-on:remove="todos.splice(index, 1)"
    ></li>
  </ul>
</div>
```

```javascript
Vue.component('todo-item', {
  template: '\
    <li>\
      {{ title }}\
      <button v-on:click="$emit(\'remove\')">Remove</button>\
    </li>\
  ',
  props: ['title']
})

new Vue({
  el: '#todo-list-example',
  data: {
    newTodoText: '',
    todos: [
      {
        id: 1,
        title: 'Do the dishes',
      },
      {
        id: 2,
        title: 'Take out the trash',
      },
      {
        id: 3,
        title: 'Mow the lawn'
      }
    ],
    nextTodoId: 4
  },
  methods: {
    addNewTodo: function () {
      this.todos.push({
        id: this.nextTodoId++,
        title: this.newTodoText
      })
      this.newTodoText = ''
    }
  }
})
```

## keep-alive

```javascript
 activated() {
     console.log('activated') // 避免频繁销毁创建，被keep-alive包含
 },
     deactivated() {
         console.log('deactivated') // 避免频繁销毁创建，被keep-alive包含
     }

 // 定义入口组件
var App = {
    template: `
<div id='appDiv'>
<keep-alive>
<global-com :father='myText' v-if='isExit' ref="global">
<p slot="one">i am slot one</p>
<p slot="two">i am slot two</p>
</global-com>
</keep-alive>
<button @click='isExit = !isExit'>组件生死</button>
</div>
`,
```



```javascript
<!-- 失活的组件将会被缓存！-->
<keep-alive>
  <component v-bind:is="currentTabComponent"></component>
</keep-alive>
```

