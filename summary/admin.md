# 应用系统

## vue-element-admin

### 环境处理

####  环境参数

存于.env.development，.env.production，.env.staging中

```javascript
# just a flag
ENV = 'production'

# base api
VUE_APP_BASE_API = '/prod-api'
```

### 图标处理

svg处理原理（https://juejin.im/post/59bb864b5188257e7a427c09）

**定义svg**

- components-SvgIcon组件，定义svg标签
- icons-index.js 全局注册SvgIcon组件，全局加载svg目录中的svg图片

```javascript
Vue.component('svg-icon', SvgIcon)

const req = require.context('./svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys().map(requireContext)
requireAll(req)
```

**使用svg**

```html
<svg-icon icon-class="user" />
```

**webpack svg-sprite-loader处理svg-sprite和svgo压缩**

```javascript
// set svg-sprite-loader
config.module
    .rule('svg')
    .exclude.add(resolve('src/icons'))
    .end()
config.module
    .rule('icons')
    .test(/\.svg$/)
    .include.add(resolve('src/icons'))
    .end()
    .use('svg-sprite-loader')
    .loader('svg-sprite-loader')
    .options({
    symbolId: 'icon-[name]'
})
    .end()
```

### 登录状态

token放在cookies中，

- 登录的时候设置vuex中的token和cookies中的token
- 获取token的时候获取cookies中的token
- 登出的时候删除cookies的token，清空vuex的token



## elementui

### vuecli3 按需引入

 https://www.jianshu.com/p/4014c68fa05e 

### el-dialog .sync

参数双向绑定

 [https://cn.vuejs.org/v2/guide/components-custom-events.html#sync-%E4%BF%AE%E9%A5%B0%E7%AC%A6](https://cn.vuejs.org/v2/guide/components-custom-events.html#sync-修饰符) 