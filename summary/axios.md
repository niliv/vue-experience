# axios

<http://www.axios-js.com/zh-cn/>

**拦截器**

```javascript
//拦截器
this.$axios.interceptors.request.use((config)=>{
    console.log('interceptorsrequest'+config)
    return config
}) // 1

this.$axios.interceptors.response.use((res)=>{
    console.log('interceptorsresponse',res)
    return res
})//4

axios.get('login',{
    transformRequest(req){
        console.log('transformRequest'+req)
        return req
    },//2
    transformResponse(data){
        console.log('transformResponse',data)
        data = 'transformResponse'+data
        return data
    }//3
})
    .then(res => {
    console.log('响应回来了',res);
    this.res1 = res.data;
})//5
// 一个失败
    .catch(function(err){
    console.log(err);
})
```

**封装request**

```javascript
import axios from 'axios'
import baseURLConfig from './config-baseURL'
import { Message } from 'element-ui'

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.baseURL = baseURLConfig.baseURL
axios.defaults.withCredentials = true

export default function request(url, type = 'GET', data = {}) {
  return new Promise((resolve, reject) => {
    let option = {
      url,
      method: type,
      validateStatus(status) {
        return (status >=200 && status < 300) || status === 400
      }
    }
    if(type.toLowerCase() === 'get') {
      option.params = data
    }else {
      option.data = data
    }
    axios(option).then(res => {
      if(res.status === 200) {
        resolve(res.data)
      }else {
        Message.error(res.data.msg)
        reject(res.data)
      }
    }).catch(err => {
      Message.error('网络异常')
      reject({ msg: '网络异常' })
    })
  })
}
```



# vue-resource

`cnpm install --save vur-resource`

`import VueResource from 'vue-resource';`

`Vue.use(VueResource)`

```javascript
getData () {
      this.$http.get('http://rest.apizza.net/mock/86eb76c00ac2c0c85b99661b338be435/evallist').then((res) => {
          console.log(res)
      }, (err) => {
          console.log(err)
      })
}
```

