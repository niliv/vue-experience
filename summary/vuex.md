# vuex

## vuex使用

```javascript

	1、src目录下面新建一个vuex的文件夹

	2、vuex 文件夹里面新建一个store.js

	3、安装vuex  

		cnpm install vuex --save

	4、在刚才创建的store.js引入vue  引入vuex 并且use vuex

		import Vue from 'vue'
		import Vuex from 'vuex'

		Vue.use(Vuex)

	5、定义数据

			/*1.state在vuex中用于存储数据*/
			var state={

			    count:1
			}
 
	6、定义方法	 mutations里面放的是方法，方法主要用于改变state里面的数据

		var mutations={

		    incCount(){

			++state.count;
		    }
		}

	暴露

		const store = new Vuex.Store({
		    state,
		    mutations
		})
		
		export default store;


组建里面使用vuex：

		1.引入 store

			 import store from '../vuex/store.js';

		2、注册

			 export default{
				data(){
				    return {               
				       msg:'我是一个home组件',
					value1: null,
				     
				    }
				},
				store,
				methods:{
				    incCount(){
				      
					this.$store.commit('incCount');   /*触发 state里面的数据*/
				    }

				}
			    }
		3、获取state里面的数据  

			this.$store.state.数据

		4、触发 mutations 改变 state里面的数据
			
			this.$store.commit('incCount');

7、优点类似计算属性   ，  改变state里面的count数据的时候会触发 getters里面的方法 获取新的值 (基本用不到)


		var getters= {
		   
		    computedCount: (state) => {
			return state.count*2
		    }
		}



	8、 Action 基本没有用

		Action 类似于 mutation，不同在于：

		Action 提交的是 mutation，而不是直接变更状态。
		Action 可以包含任意异步操作。
		


		var actions= {
		    incMutationsCount(context) {    /*因此你可以调用 context.commit 提交一个 mutation*/
		      
		      
			context.commit('incCount');    /*执行 mutations 里面的incCount方法 改变state里面的数据*/


		    }
		}

	


	暴露

		const store = new Vuex.Store({
		    state,
		    mutations,
		    getters,
		    actions
		})


		
		export default store;



3、获取state里面的数据  

			this.$store.state.数据



		4、触发 mutations 改变 state里面的数据
			
			this.$store.commit('incCount');


		5、触发 actions里面的方法
			
			this.$store.dispatch('incCount');


		6、{{this.$store.getters.computedCount}}  获取 getters里面方法返回的的数据
```





## 分模块管理

```javascript
import Vue from 'vue'
import Vuex from 'vuex'
import notebook from './modules/notebook'
import note from './modules/note'
import trash from './modules/trash'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    notebook,
    note,
    trash,
    user
  }
})
```

## 项目中使用

**notebooklist**

```javascript
<template>
  <div class="detail" id="notebook-list">
    <header>
      <a href="#" class="btn" @click.prevent="onCreate"
        ><i class="iconfont icon-plus"></i> 新建笔记本</a
      >
    </header>
    <main>
      <div class="layout">
        <h3>笔记本列表({{ notebooks.length }})</h3>
        <div class="book-list">
          <router-link
            v-for="notebook in notebooks"
            :key="notebook.id"
            :to="`/note?notebookId=${notebook.id}`"
            class="notebook"
          >
            <div>
              <span class="iconfont icon-notebook"></span> {{ notebook.title }}
              <span>{{ notebook.noteCounts }}</span>
              <span class="action" @click.stop.prevent="onEdit(notebook)"
                >编辑</span
              >
              <span class="action" @click.stop.prevent="onDelete(notebook)"
                >删除</span
              >
              <span class="date">{{ notebook.createdAtFriendly }}</span>
            </div>
          </router-link>
        </div>
      </div>
    </main>
  </div>
</template>
<script>

import {friendlyDate} from '@/helpers/util'
import {mapState, mapActions, mapGetters} from 'vuex'

//window.Notebooks = Notebooks

export default {
  data () {
    return {}
  },

  created () {
    this.checkLogin({path: '/login'})
    this.getNotebooks()
  },

  computed: {
    ...mapGetters(['notebooks'])
  },

  methods: {
    ...mapActions([
      'getNotebooks',
      'addNotebook',
      'updateNotebook',
      'deleteNotebook',
      'checkLogin'
    ]),

    onCreate () {
      this.$prompt('输入新笔记本标题', '创建笔记本', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        inputPattern: /^.{1,30}$/,
        inputErrorMessage: '标题不能为空，且不超过30个字符'
      }).then(({value}) => {
        this.addNotebook({title: value})
      })
    },

    onEdit (notebook) {
      let title = ''
      this.$prompt('输入新笔记本标题', '修改笔记本', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        inputPattern: /^.{1,30}$/,
        inputValue: notebook.title,
        inputErrorMessage: '标题不能为空，且不超过30个字符'
      }).then(({value}) => {
        this.updateNotebook({notebookId: notebook.id, title: value})
      })
    },

    onDelete (notebook) {
      this.$confirm('确认要删除笔记本吗', '删除笔记本', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        this.deleteNotebook({notebookId: notebook.id})
      })
    }
  }
}
</script>

<style lang="less" scoped>
@import url(../assets/css/notebook-list.less);
</style>

```



```javascript
import Notebook from '@/apis/notebooks'
import { Message } from 'element-ui'

const state = {
  notebooks: null,
  curBookId: null
}

const getters = {
  notebooks: state => state.notebooks || [],

  curBook: state => {
    if(!Array.isArray(state.notebooks)) return {}
    if(!state.curBookId) return state.notebooks[0] || {}
    return state.notebooks.find(notebook => notebook.id == state.curBookId) || {}
  }
}

const mutations = {
  setNotebooks(state, payload) {
    state.notebooks = payload.notebooks
  },

  addNotebook(state, payload) {
    state.notebooks.unshift(payload.notebook)
  },

  updateNotebook(state, payload) {
    let notebook = state.notebooks.find(notebook => notebook.id == payload.notebookId) || {}
    notebook.title = payload.title
  },

  deleteNotebook(state, payload) {
    state.notebooks = state.notebooks.filter(notebook => notebook.id != payload.notebookId)
  },

  setCurBook(state, payload) {
    state.curBookId = payload.curBookId
  }
}

const actions = {
  getNotebooks({ commit, state }) {
    if(state.notebooks !== null) return Promise.resolve()
    return Notebook.getAll()
      .then(res => {
        commit('setNotebooks', { notebooks: res.data })
      })
  },

  addNotebook({ commit }, payload) {
    return Notebook.addNotebook({ title: payload.title })
      .then(res => {
        console.log('add success...', res)
        commit('addNotebook', { notebook: res.data })
        Message.success(res.msg)
      })
  },

  updateNotebook({ commit }, payload) {
    return Notebook.updateNotebook(payload.notebookId, { title: payload.title })
      .then(res => {
        commit('updateNotebook', { notebookId: payload.notebookId, title: payload.title })
        Message.success(res.msg)
      })
  },

  deleteNotebook({ commit }, payload) {
    return Notebook.deleteNotebook(payload.notebookId)
      .then(res => {
        commit('deleteNotebook', { notebookId: payload.notebookId })
        Message.success(res.msg)
      })
  } 
}


export default {
  state,
  getters,
  mutations,
  actions
}
```

**note**

```javascript
<template>
  <div id="note" class="detail">
    <note-sidebar @update:notes="val => (notes = val)"></note-sidebar>
    <div class="note-detail">
      <div class="note-empty" v-show="!curBook.id">请创建笔记本后</div>
      <div class="note-empty" v-show="!curNote.id">选择或创建笔记</div>
      <div class="note-detail-ct" v-show="curNote.id">
        <div class="note-bar">
          <span> 创建日期: {{ curNote.createdAtFriendly }}</span>
          <span> 更新日期: {{ curNote.updatedAtFriendly }}</span>
          <span> {{ statusText }}</span>
          <span class="iconfont icon-delete" @click="onDeleteNote"></span>
          <span
            class="iconfont"
            :class="isShowPreview ? 'icon-edit' : 'icon-eye'"
            @click="isShowPreview = !isShowPreview"
          ></span>
        </div>
        <div class="note-title">
          <input
            type="text"
            :value="curNote.title"
            @input="onUpdateNote"
            @keydown="statusText = '正在输入...'"
            placeholder="输入标题"
          />
        </div>
        <div class="editor">
          <codemirror
            v-model="curNote.content"
            :options="cmOptions"
            v-show="!isShowPreview"
            @input="onUpdateNote"
            @inputRead="statusText = '正在输入...'"
          ></codemirror>
          <!--  <textarea v-show="isShowPreview"  v-model:value="curNote.content" @input="onUpdateNote" @keydown="statusText='正在输入...'" placeholder="输入内容, 支持 markdown 语法"></textarea>-->
          <div
            class="preview markdown-body"
            v-html="previewContent"
            v-show="isShowPreview"
          ></div>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
import NoteSidebar from '@/components/NoteSidebar'
import _ from 'lodash'
import MarkdownIt from 'markdown-it'
import {mapState, mapGetters, mapMutations, mapActions} from 'vuex'
import {codemirror} from 'vue-codemirror'
import 'codemirror/lib/codemirror.css'
import 'codemirror/mode/markdown/markdown.js'
import 'codemirror/theme/neat.css'

let md = new MarkdownIt()


export default {
  components: {
    NoteSidebar,
    codemirror
  },

  data () {
    return {
      statusText: '笔记未改动',
      isShowPreview: false,
      cmOptions: {
        tabSize: 4,
        mode: 'text/x-markdown',
        theme: 'neat',
        lineNumbers: false,
        line: true,
        // more codemirror options, 更多 codemirror 的高级配置...
      }
    }
  },

  created () {
    this.checkLogin({path: '/login'})
  },

  computed: {
    ...mapGetters([
      'notes',
      'curNote',
      'curBook'
    ]),

    previewContent () {
      return md.render(this.curNote.content || '')
    }
  },

  methods: {
    ...mapMutations([
      'setCurNote'
    ]),

    ...mapActions([
      'updateNote',
      'deleteNote',
      'checkLogin'
    ]),

    onUpdateNote: _.debounce(function () {
      if (!this.curNote.id) return
      this.updateNote({noteId: this.curNote.id, title: this.curNote.title, content: this.curNote.content})
        .then(data => {
          this.statusText = '已保存'
        }).catch(data => {
          this.statusText = '保存出错'
        })

    }, 3000),

    onDeleteNote () {
      this.deleteNote({noteId: this.curNote.id})
        .then(data => {
          this.$router.replace({path: '/note'})
        })
    }

  },

  beforeRouteUpdate (to, from, next) {
    this.setCurNote({curNoteId: to.query.noteId})
    next()
  }
}
</script>

<style lang="less">
@import url(../assets/css/note-detail.less);

#note {
  display: flex;
  align-items: stretch;
  background-color: #fff;
  flex: 1;
}
</style>
```



```javascript
import Note from '@/apis/notes'
import { Message } from 'element-ui'

const state = {
  notes: null,
  curNoteId: null
}

const getters = {
  notes: state => state.notes || [],

  curNote: state => {
    if(!Array.isArray(state.notes)) return { title: '', content: '' }
    if(!state.curNoteId) return state.notes[0] || { title: '', content: '' }
    return state.notes.find(note => note.id == state.curNoteId) || { title: '', content: '' }
  }
}

const mutations = {
  setNote(state, payload) {
    state.notes = payload.notes
  },

  addNote(state, payload) {
    state.notes.unshift(payload.note)
  },

  updateNote(state, payload) {
    let note = state.notes.find(note => note.id === payload.noteId) || {}
    note.title = payload.title
    note.content = payload.content
  },

  deleteNote(state, payload) {
    state.notes = state.notes.filter(note => note.id !== payload.noteId)
  },

  setCurNote(state, payload = {}) {
    state.curNoteId = payload.curNoteId
  }

}

const actions = {
  getNotes({ commit }, { notebookId }) {
    return Note.getAll({ notebookId })
      .then(res => {
        commit('setNote', { notes: res.data })
      })
  },

  addNote({ commit }, { notebookId, title, content }) {
   return Note.addNote({ notebookId }, { title, content })
      .then(res => {
        console.log('add success...', res)
        commit('addNote', { note: res.data })
      })
  },

  updateNote({ commit }, { noteId, title, content }) {
    return Note.updateNote({ noteId }, { title, content })
      .then(res => {
        commit('updateNote', { noteId, title, content })
      })
  },

  deleteNote({ commit }, { noteId }) {
    return Note.deleteNote({ noteId })
      .then(res => {
        commit('deleteNote', { noteId })
        Message.success(res.msg)
      })
  } 
}


export default {
  state,
  getters,
  mutations,
  actions
}
```

### getter computed

```javascript
<script>
  import { mapGetters, mapActions } from 'vuex'

  export default {
    data() {
      return {}
    },

    created() {
      this.setUser()
    },

    methods: {
      ...mapActions({
        'setUser': 'checkLogin'
      })
    },

    computed: {
      ...mapGetters([
        'username',
        'slug'
        ])
    }
  }
</script>图
```

## 图

![](https://niliv-technology-1252830662.cos.ap-chengdu.myqcloud.com/web/Snipaste_2019-11-25_20-35-03.png)

## vuex和缓存

```javascript
const state = {
  shop: {}
}

const getters = {
  shop: () => JSON.parse(sessionStorage.getItem('shop'))
}

const mutations = {
  setShop(state, payload) {
    sessionStorage.setItem('shop', JSON.stringify(payload))
    state.shop = payload
  }
}

const actions = {
  setShop({ commit }, payload) {
    commit('setShop', payload)
  }
}

export default {
  getters,
  state,
  mutations,
  actions
}

```

```javascript
import Vue from 'vue'
import Vuex from 'vuex'
import shop from './modules/shop'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    shop
  }
})

export default store

```

```vue
<template>
  <div>
    <p>this is home</p>
    <button @click="show">设置</button>
    <button @click="getShop">获取</button>
  </div>
</template>

<script>
import { mapGetters, mapActions } from 'vuex'
export default {
  computed: {
    ...mapGetters(['shop'])
  },
  methods: {
    ...mapActions(['setShop']),
    show() {
      this.setShop({ id: 1, name: '喵姐店铺' })
      //this.$store.dispatch('setShop', { id: 1, name: '喵姐店铺' })
    },
    getShop() {
      console.log(JSON.parse(sessionStorage.getItem('shop')))
      console.log(this.shop)
    }
  }
}
</script>

<style lang="scss" scoped></style>

```

