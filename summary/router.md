# router

### 路由权限控制

```javascript
var router = new  VueRouter({
    routes:[
        {
            path: '/',
            redirect: {
                name:'login'
            }
        },
        {
            name: 'login',
            path:'/login',
            component: Login,
            children: [
                {
                    name: 'login.woman',
                    path:'woman',
                    component: Woman
                }
            ]
        },
        {
            name: 'register',
            path:'/register/:name',
            component: Register,
            meta:{
                isChecked:true
            },
            children: [
                {
                    name: 'register.man',
                    path:'man',
                    component: Man
                }
            ]
        }
    ]
})

router.beforeEach(function(to,from,next){
    if(!to.meta.isChecked){
        next();
    }
    else{
        if(isLogin){
            next()
        }else{
            alert('请先登录')
            next({name:'login'})
        }
    }

})
```

### 路由使用

https://router.vuejs.org/


vue路由配置：


	1.安装 
	
	npm install vue-router  --save   / cnpm install vue-router  --save


	2、引入并 Vue.use(VueRouter)   (main.js)
	 
		import VueRouter from 'vue-router'
	
		Vue.use(VueRouter)
	3、配置路由
		1、创建组件 引入组件
			import Home from './components/Home';
			import News from './components/News';
		2、定义路由  （建议复制s）
			const routes = [
			  { path: '/foo', component: Foo },
			  { path: '/bar', component: Bar },
			  { path: '*', redirect: '/home' }   /*默认跳转路由*/
			]
		3、实例化VueRouter
			const router = new VueRouter({
			  routes // （缩写）相当于 routes: routes
			})
		4、挂载
			new Vue({
	          el: '#app',
	          router，
	          render: h => h(App)
			})
		5 、根组件的模板里面放上这句话   <router-view></router-view> 
		6、路由跳转
			<router-link to="/foo">Go to Foo</router-link>
		 	<router-link to="/bar">Go to Bar</router-link>	  

### 路由模块化

```javascript
import Vue from "vue";
import VueRouter from "vue-router";
import Content from "../components/Content";
import Home from "../components/Home";
import News from "../components/News";
import User from '../components/User';
//使用插件
Vue.use(VueRouter)
//定义路由规则
const routes = [{
    path: "/home",
    component: Home
  },
  {
    path: "/news",
    component: News,
    name: 'news'
  },
  {
    path: "/content/:id",
    component: Content
  },
  {
    path: "/user",
    component: User
  },
  {
    path: "*", //没有匹配到
    redirect: "/home"
  } /*默认跳转路由*/
];
//定义路由对象
const router = new VueRouter({
  mode: 'history',
  routes // （缩写）相当于 routes: routes
});
//导出路由
export default router;
```



### 传值

```javascript
<ul>
      <li v-for="(item, key) in list" :key="key">
        <router-link :to="'/content/' + key">{{
          item.evaluate_content
        }}</router-link>
      </li>
    </ul>
    <br />
    <hr />
    <ul>
      <li v-for="(item, key) in list" :key="key">
        <router-link :to="'/content?id=' + key">{{
          item.evaluate_content
        }}</router-link>
      </li>
</ul>
```

```javascript
mounted () {
    console.log(this.$route.params)
    console.log(this.$route.query)
  },
```

### 编程式跳转传值

```javascript
this.$router.push({path: 'news',query:{name:'jack'}})
this.$router.push({name:'news',params:{id:1}})
//接受
{{$route.params.id}}
```

### history模式及nginx配置

```javascript
const router = new VueRouter({
  mode: 'history',
  routes // （缩写）相当于 routes: routes
});
//nginx
location /spzs {
    alias /usr/share/nginx/html/spzs/;
    try_files $uri $uri/ /spzs/index.html;
    index  index.html;
}
```

### 没有匹配到

```javascript
{
    path: "*", //没有匹配到
    redirect: "/home"
} /*默认跳转路由*/
```

### 常规路由代码

```javascript
/* Layout */
/* eslint-disable */
import Layout from "@/layout";
import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export const constantRoutes = [
  {
    path: "/login",
    component: () => import("@/views/login/index"),
    hidden: true
  },

  {
    path: "/404",
    component: () => import("@/views/404"),
    hidden: true
  },
  {
    path: "/notice",
    component: () => import("@/views/notice"),
    hidden: true
  }
  {
    path: "/",
    component: Layout,
    redirect: "/batch"
  },

  {
    path: "/batch",
    component: Layout,
    redirect: "/batch/export",
    name: "batch",
    meta: { title: "导入修改", icon: "form" },
    children: [
      {
        path: "export",
        name: "export",
        component: () => import("@/views/batch/export"),
        meta: { title: "导出数据模板", icon: "excel" }
      },
      {
        path: "import",
        name: "Import",
        component: () => import("@/views/batch/import"),
        meta: { title: "导入修改数据", icon: "example" }
      },
      {
        path: "recordLog/:id(\\d+)",
        component: () => import("@/views/batch/recordLog"),
        name: "RecordLog",
        meta: {
          title: "商品更新详情",
          noCache: true,
          activeMenu: "/example/record"
        },
        hidden: true
      },
      {
        path: "record",
        name: "Record",
        component: () => import("@/views/batch/record"),
        meta: { title: "商品修改记录", icon: "list" }
      }
    ]
  },
  {
    path: "/interact",
    component: Layout,
    redirect: "/interact/feedback",
    children: [
      {
        path: "feedback",
        component: () => import("@/views/interact/feedback"),
        name: "Feedback",
        meta: { title: "我要反馈", icon: "guide" }
      }
    ]
  }
];

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [...constantRoutes]
});
export default router;

```



