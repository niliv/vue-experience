#  技巧

### 参考

初始化项目 `vue init webpack` `vue create ***`

vscode插件：vetur  eslint  prettier

json-server: 

npm i -g json-server

创建db.json

命令行 json-server --watch ./db.json

[代码格式和规范](<https://segmentfault.com/ls/1650000019112557/l/1500000019112559>)

原型链：app._proto_ === Vue.prototype

[lodash](<https://www.lodashjs.com/>)

[vue编写指南](https://cn.vuejs.org/v2/style-guide/)



### 数组里添加对象

```java
// 添加unit，菜单项目到list列表
      // let obj = new Object()
      // ... 扩展运算符
      // obj.name = this.unit.name ..
      this.lists.push({ ...this.unit })
      this.$store.commit('setList', this.lists)
      this.unit.name = ''
      this.unit.url = ''
      this.unit.type = ''
      this.unit.price = ''
```

因为后面对象有改动，所以添加进数组的时候需要新生成一个对象

### 设置数组中的值

```javascript
<template>
  <div>
    <div class="left">
      左侧是下单
      <ul>
        <li v-for="(item,index) in lists" :key="'order'+ index">
          <span>{{item.name}}-{{item.price}}</span>
          <button type="button" @click="minus(item, index)">-</button>
          <span>{{typeof item.num === 'undefined' ? 0: item.num}}</span>
          <button type="button" @click="add(item, index)">+</button>
        </li>
      </ul>
    </div>
    <div class="right">
      计算部分
      <ul>
        <li
          v-for="(item,index) in orders"
          :key="'order' + index"
        >菜品名称：{{item.name}}-菜品单价：{{item.price}}-单项总价：{{item.price * item.num}}</li>
      </ul>
      <p>菜单总价：{{total}}</p>
    </div>
  </div>
</template>

<script>
import _ from 'lodash'
export default {
  name: 'order',
  data () {
    return {
      lists: this.$store.state.lists
    }
  },
  // mounted () {
  //   let arr = _.filter([1, 2, 3], (item) => item > 1)
  //   console.log(arr)
  // },
  computed: {
    orders () {
      return _.filter(this.lists, (item) => typeof item.num !== 'undefined' && item.num > 0)
    },
    total () {
      let sum = 0
      _.each(this.orders, (item) => {
        sum += item.price * item.num
      })
      return sum
    }
  },
  methods: {
    minus (item, index) {
      if (typeof item.num === 'undefined') {
        item.num = 0
      }
      item.num--
      if (item.num < 0) {
        item.num = 0
      }
      this.$set(this.lists, index, item)
    },
    add (item, index) {
      if (typeof item.num === 'undefined') {
        item.num = 0
      }
      item.num++
      if (item.num > 100) {
        item.num = 100
      }
      this.$set(this.lists, index, item)
    }
  }
}
</script>

<style lang="scss" scoped>
</style>

```





### methods,watch,computed

1. `computed`属性的结果会被缓存，除非依赖的响应式属性变化才会重新计算。主要当作属性来使用；
2. `methods`方法表示一个具体的操作，主要书写业务逻辑；
3. `watch`一个对象，键是需要观察的表达式，值是对应回调函数。主要用来监听某些特定数据的变化，从而进行某些具体的业务逻辑操作；可以看作是`computed`和`methods`的结合体；



### v-if v-show

`v-if` 是“真正”的条件渲染，因为它会确保在切换过程中条件块内的事件监听器和子组件适当地被销毁和重建。

`v-if` 也是**惰性的**：如果在初始渲染时条件为假，则什么也不做——直到条件第一次变为真时，才会开始渲染条件块。

相比之下，`v-show` 就简单得多——不管初始条件是什么，元素总是会被渲染，并且只是简单地基于 CSS 进行切换。

一般来说，`v-if` 有更高的切换开销，而 `v-show` 有更高的初始渲染开销。因此，如果需要非常频繁地切换，则使用 `v-show` 较好；如果在运行时条件很少改变，则使用 `v-if` 较好。

**不推荐**同时使用 `v-if` 和 `v-for`

### v-for

为了给 Vue 一个提示，以便它能跟踪每个节点的身份，从而重用和重新排序现有元素，你需要为每项提供一个唯一 `key` 属性：

不要使用对象或数组之类的非原始类型值作为 `v-for` 的 `key`。用字符串或数类型的值取而代之。



### 数组变异方法

Vue 包含一组观察数组的变异方法，所以它们也将会触发视图更新

- `push()`
- `pop()`
- `shift()`
- `unshift()`
- `splice()`
- `sort()`
- `reverse()`

变异方法 (mutation method)，顾名思义，会改变被这些方法调用的原始数组



### 数据更改检测

**数组**

由于 JavaScript 的限制，Vue 不能检测以下变动的数组：

1. 当你利用索引直接设置一个项时，例如：`vm.items[indexOfItem] = newValue`
2. 当你修改数组的长度时，例如：`vm.items.length = newLength`

```javascript
var vm = new Vue({
  data: {
    items: ['a', 'b', 'c']
  }
})
vm.items[1] = 'x' // 不是响应性的
vm.items.length = 2 // 不是响应性的
```

```javascript
Vue.set(vm.items, indexOfItem, newValue)
vm.$set(vm.items, indexOfItem, newValue)
vm.items.splice(newLength)
```

**对象**

由于 JavaScript 的限制，**Vue 不能检测对象属性的添加或删除**

```javascript
var vm = new Vue({
  data: {
    a: 1
  }
})
// `vm.a` 现在是响应式的

vm.b = 2
// `vm.b` 不是响应式的
```

```javascript
Vue.set(vm.userProfile, 'age', 27)
vm.$set(vm.userProfile, 'age', 27)
```

有时你可能需要为已有对象赋予多个新属性

```javascript
vm.userProfile = Object.assign({}, vm.userProfile, {
  age: 27,
  favoriteColor: 'Vue Green'
})
```

### 计算属性 过滤

有时，我们想要显示一个数组的过滤或排序副本，而不实际改变或重置原始数据。在这种情况下，可以创建返回过滤或排序数组的计算属性

```javascript
<li v-for="n in evenNumbers">{{ n }}</li>
data: {
  numbers: [ 1, 2, 3, 4, 5 ]
},
computed: {
  evenNumbers: function () {
    return this.numbers.filter(function (number) {
      return number % 2 === 0
    })
  }
}

在计算属性不适用的情况下 (例如，在嵌套 v-for 循环中) 你可以使用一个 method 方法
<li v-for="n in even(numbers)">{{ n }}</li>
data: {
  numbers: [ 1, 2, 3, 4, 5 ]
},
methods: {
  even: function (numbers) {
    return numbers.filter(function (number) {
      return number % 2 === 0
    })
  }
}
```



### 事件修饰

在事件处理程序中调用 `event.preventDefault()` 或 `event.stopPropagation()` 是非常常见的需求。尽管我们可以在方法中轻松实现这点，但更好的方式是：方法只有纯粹的数据逻辑，而不是去处理 DOM 事件细节。

为了解决这个问题，Vue.js 为 `v-on` 提供了**事件修饰符**。之前提过，修饰符是由点开头的指令后缀来表示的。

- `.stop`
- `.prevent`
- `.capture`
- `.self`
- `.once`
- `.passive`

[vue事件](https://cn.vuejs.org/v2/guide/events.html)



### 表单

[vue表单元素](https://cn.vuejs.org/v2/guide/forms.html)

### 路由

router-link

```javascript
<router-link to="/bar">Go to Bar</router-link>
<router-link to="/user/yuankun">Go to yuankun</router-link>
<router-link to="/user/kk">Go to kk</router-link>
<router-link :to="{ name: 'user', params: { id: 123 }}">User</router-link>
```

router.push 可以回退

```javascript
// 字符串
router.push('home')

// 对象
router.push({ path: 'home' })

// 命名的路由
router.push({ name: 'user', params: { userId: 123 }})

// 带查询参数，变成 /register?plan=private
router.push({ path: 'register', query: { plan: 'private' }})

注意：如果提供了 path，params 会被忽略，上述例子中的 query 并不属于这种情况。取而代之的是下面例子的做法，你需要提供路由的 name 或手写完整的带有参数的 path

const userId = 123
router.push({ name: 'user', params: { userId }}) // -> /user/123
router.push({ path: `/user/${userId}` }) // -> /user/123
// 这里的 params 不生效
router.push({ path: '/user', params: { userId }}) // -> /user
```

router.replace 不能回退

router.go  == window.history.go

**$router会被注入到所有的组件内**

在 2.2.0+，可选的在 `router.push` 或 `router.replace` 中提供 `onComplete` 和 `onAbort` 回调作为第二个和第三个参数。这些回调将会在导航成功完成 (在所有的异步钩子被解析之后) 或终止 (导航到相同的路由、或在当前导航完成之前导航到另一个不同的路由) 的时候进行相应的调用。

**注意：**如果目的地和当前路由相同，只有参数发生了改变 (比如从一个用户资料到另一个 `/users/1` -> `/users/2`)，你需要使用 [`beforeRouteUpdate`](https://router.vuejs.org/zh/guide/essentials/dynamic-matching.html#响应路由参数的变化) 来响应这个变化 (比如抓取用户信息)

路由传值

```javascript
const User = {
    template: '<div>User {{ $route.params.id }}</div>'
}
const Reigster = {
    props: ['id'],
    template: '<div>Reg {{ id }}</div>'
}
{
    path: '/username/:id',
        redirect: {
            name: 'user'
        }
},
    {
        path: '/reg/:id',
            name: 'reg',
                component: Reigster,
                    props: true
    }
```

[导航守卫](<https://router.vuejs.org/zh/guide/advanced/navigation-guards.html>)

### 自定义swiper组件

```javascript
<template>
  <mt-swipe :auto="0">
    <mt-swipe-item v-for="img in imgs" :key="img.id">
      <img :src="img.path" alt="no" />
    </mt-swipe-item>
  </mt-swipe>
</template>
<script>
export default {
  name: 'my-swipe',
  data () {
    return {
      imgs: []
    }
  },
  props: ['url'],
  created () {
    this.$axios.get(this.url)
      .then(res => {
        this.imgs = res.data
      })
  }
}
//main
import MySwipe from '@/components/common/MySwipe'
Vue.component(MySwipe.name, MySwipe)
//home
<my-swipe url="getlunbo" />
```

### 自定义留言组件

```javascript
<template>
    <div>
        <section>
            <textarea v-model="newComment" ref="textarea" ></textarea>
        </section>
        <section>
            <mt-button type="primary" size="large" @click.native="sendComment">发表评论</mt-button>
        </section>
        <section>
            <ul>
                <li v-for="item in comments.newComments" :key="item.id">
                    {{item.person}} | {{item.content}} | {{item.createTime | formatDate}}
                </li>
            </ul>
        </section>
        <section>
            <mt-button type="primary" size="large" plain @click.native="loadMore" v-show="this.comments.isFull">加载更多</mt-button>
        </section>
    </div>
</template>
<script>
export default {
  name: 'my-comment',
  data () {
    return {
      newComment: '',
      page: 1,
      comments: {}
    }
  },
  props: ['relativeId', 'relativeType'],
  created () {
    this.getComments()
  },
  methods: {
    sendComment () {
      if (!this.$refs.textarea.value) {
        this.$toast({
          message: '请填写评论'
        })
      }
    },
    getComments () {
      console.log('getComments/' + this.relativeId + '/' + this.relativeType + '/' + this.page)
      this.$axios.get('getComments/' + this.relativeId + '/' + this.relativeType + '/' + this.page)
        .then(res => {
          console.log(res)
          this.comments = res.data
        })
        .catch(err => console.log(err))
    },
    loadMore () {
      this.page++
      this.getComments()
    }
  }
}
</script>
```



### 插入组件

```javascript
<template>
  <div id="app">
    <sidebar></sidebar>
    <router-view></router-view>
  </div>
</template>

import sidebar from '@/components/Sidebar.vue'
export default {
  name: 'App',
  components: {
    sidebar
  }
}
```

```javascript
//引入自定义或第三方组件
Vue.component(MyUl.name, MyUl)
Vue.component(MyLi.name, MyLi)
Vue.component(Comment.name, Comment)
Vue.component(MySwipe.name, MySwipe)
//引入插件
Vue.use(MintUI)
Vue.use(VuePreview)
```

### 自定义导航

```javascript
<my-ul>
    <my-li v-for="(grid,index) in grids" :key="index">
        <router-link :to="grid.router">
            <div :class="grid.itemClass">
                {{grid.title}}
                    </div>
		</router-link>
	</my-li>
</my-ul>

grids: [
    {
        title: '新闻资讯',
        itemClass: 'item item_news',
        router: {name: 'news.list'}
    },
    {
        title: '图文分享',
        itemClass: 'item item_share',
        router: {name: 'photo.list', params: {categoryId: 0}}
    },
    {
        title: '商品展示',
        itemClass: 'item item_product',
        router: {name: 'goods.list'}
    },
    {
        title: '留言反馈',
        itemClass: 'item item_feedback',
        router: {name: 'news.list'}
    },
    {
        title: '搜索资讯',
        itemClass: 'item item_searchnews',
        router: {name: 'news.list'}
    },
    {
        title: '联系我们',
        itemClass: 'item item_contact',
        router: {name: 'news.list'}
    }
]

.item{
    background-repeat: no-repeat;
    background-position: top center;
    text-align: center;
    height: 100px;
    line-height: 170px;
    background-size: 35%;
    vertical-align: bottom;
}
.item_news{
  background-image: url("../../assets/img/news.png");
}
.item_share{
  background-image: url("../../assets/img/share.png");
}
.item_product{
  background-image: url("../../assets/img/product.png");
}
```



### 全局filter

```javascript
import Moment from 'moment'
Vue.filter('formatDate', function (val, str) {
  if (val) {
    return Moment(val).format(str)
  }
  return '--'
})
Vue.filter('formatDate', function (val) {
  if (val) {
    return Moment(val).fromNow()
  }
  return '--'
})
Vue.filter('convertStr', (val, num) => {
  if (val && val.length > num) {
    return val.substring(0, num) + '......'
  }
  return val
})
```



### 引入全局css

```javascript
//main.js
import './assets/css/global.css'
import './assets/ttf/iconfont.css'
```

### fontclass

```css
APP.vue
@import "//at.alicdn.com/t/font_496303_kqrjhri8l25d0a4i.css";
siderbar.vue
<router-link to="/note/1" title="笔记"><i class="iconfont icon-note"></i></router-link>
.iconfont {
    color: #fff;
}
```

### less

安装less less-load

### vue动画

```html
<transition name="slide">
    <div v-bind:class="{show: isShowRegister}" class="register">
        <input type="text" v-model="register.username" placeholder="用户名">
        <input type="password" v-model="register.password" @keyup.enter="onRegister" placeholder="密码">
        <p v-bind:class="{error: register.isError}"> {{register.notice}}</p>
        <div class="button" @click="onRegister">创建账号</div>
    </div>
</transition>
```

```css
.login,.register {
      
    padding: 0px 20px;
    border-top: 1px solid #eee;
    height: 0;
    overflow: hidden;
    transition: height .4s;

    &.show {
        height: 193px;
    }
```

### axios

<https://www.npmjs.com/package/axios>

```javascript
import axios from 'axios';
import baseURLConfig from './config-baseURL';
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencode';
axios.defaults.baseURL = baseURLConfig.baseURL;
axios.defaults.withCredentials = true;
```

`axios.defaults.withCredentials = true;`允许跨域cookies

**全局引入axios**

```javascript
//main.js
import Axios from 'axios'
Axios.defaults.baseURL = 'http://localhost:8888/'
Axios.defaults.withCredentials = true
Vue.prototype.$axios = Axios
```



### 封装axios请求

```javascript
//request.js
import axios from 'axios'
import baseURLConfig from './config-baseURL'
import { Message } from 'element-ui'

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.baseURL = baseURLConfig.baseURL
axios.defaults.withCredentials = true

// 配置请求拦截器
Axios.interceptors.request.use(config => {
  MintUI.Indicator.open({
    text: '加载中'
  })
  return config
})
// 配置响应拦截器
Axios.interceptors.response.use(response => {
  MintUI.Indicator.close()
  return response
})

export default function request(url, type = 'GET', data = {}) {
  return new Promise((resolve, reject) => {
    let option = {
      url,
      method: type,
      validateStatus(status) {
        return (status >=200 && status < 300) || status === 304
      }
    }
    if(type.toLowerCase() === 'get') {
      option.params = data
    }else {
      option.data = data
    }
    axios(option).then(res => {
      if(res.status === 200) {
        resolve(res.data)
      }else {
        Message.error(res.data.msg)
        reject(res.data)
      }
    }).catch(err => {
      Message.error('网络异常')
      reject({ msg: '网络异常' })
    })
  })
}

```

### 封装API

```javascript
import request from "../helpers/request";

const URL = {
  REGISTER: '/auth/register',
  LOGIN: '/auth/login',
  LOGOUT: '/auth/logout',
  GET_INFO: '/auth'
}
export default {
  register({
    username,
    password
  }) {
    return request(URL.REGISTER, 'POST', {
      username,
      password
    })
  },
  login({
    username,
    password
  }) {
    return request(URL.LOGIN, 'POST', {
      username,
      password
    })
  },
  logout() {
    return request(URL.LOGOUT)
  },
  getInfo() {
    return request(URL.GET_INFO)
  }
}

Auth.login({username:this.register.username,password:this.register.password}).then(data=>{
    console.log(data)
}).catch(err=>console.log(err))
```

```javascript
import request from '@/helpers/request'
import {friendlyDate} from '@/helpers/util'

const URL = {
  GET: '/notebooks',
  ADD: '/notebooks',
  UPDATE: '/notebooks/:id',
  DELETE: '/notebooks/:id'
}

export default {
  getAll() {
    return new Promise((resolve, reject) => {
      request(URL.GET)
        .then(res => {
          res.data = res.data.sort((notebook1, notebook2) => notebook1.createdAt < notebook2.createdAt)
          res.data.forEach(notebook=>{
            notebook.createdAtFriendly = friendlyDate(notebook.createdAt)
            notebook.updatedAtFriendly = friendlyDate(notebook.updatedAt)
          })
          resolve(res)
        }).catch(err => {
          reject(err)
        })
    })
  },

  updateNotebook(notebookId, { title = '' } = { title: '' }) {
    return request(URL.UPDATE.replace(':id', notebookId), 'PATCH', { title })
  },

  deleteNotebook(notebookId) {
    return request(URL.DELETE.replace(':id', notebookId), 'DELETE')
  },

  addNotebook({ title = ''} = { title: ''}) {
    return new Promise((resolve, reject) => {
      request(URL.ADD, 'POST', { title })
        .then(res => {
          res.data.createdAtFriendly = friendlyDate(res.data.createdAt)
          res.data.updatedAtFriendly = friendlyDate(res.data.updatedAt)
          resolve(res)
        }).catch(err => {
          reject(err)
        })
    })
  }

}
```





### 管理开发生产环境API链接

```javascript
//mock.config.js
const fs = require('fs')
const path = require('path')

const mockBaseURL = 'http://note-server.hunger-valley.com'
const realBaseURL = 'http://note-server.hunger-valley.com';

exports.config = function ({
  isDev = true
} = {
  isDev: true
}) {
  let fileTxt = `
    module.exports = {
      baseURL: '${isDev ? mockBaseURL : realBaseURL}'
    }
  `
  fs.writeFileSync(path.join(__dirname, '../src/helpers/config-baseURL.js'), fileTxt)
}

//webpack.dev.conf.js
require('./mock.config').config({
  isDev: true
});
//webpack.prod.conf.js
require('./mock.config').config({
  isDev: false
});

```

### nextTick

$nextTick可以获取到渲染后的dom，经常放在created中用于回调

```javascript
this.$nextTick(() => {
    console.log('hello')
    document.querySelectorAll('#app li')[2].innerText = 'hello'
    console.log(this.name)
})
```

### 组件事件传递

```javascript
//bus.js
import Vue from 'vue';
export default new Vue()
//login
Auth.login({username: this.login.username, password: this.login.password}).then(data => {
    console.log(data)
    this.login.isError = false;
    this.login.notice = '';
    this.$router.push({path: 'notebooks'})
    Bus.$emit('userInfo', {username: this.login.username})
//avatar
 created () {
    Bus.$on('userInfo', user => {
      this.username = user.username;
    })
```

```javascript
//EventBus
import Vue from 'vue'
let EventBus = new Vue()
export default EventBus
//触发
EventBus.$emit('addShopCart', obj.getTotalCount())
//接收
EventBus.$on('addShopCart', data => {
    this.num = data
})
```

### 事件防止冒泡和阻止默认事件

```html
<span class="action" @click.stop.prevent="onDelete(notebook)">删除</span>
```

### 增删改查

```javascript
onCreate () {
    let title = window.prompt("创建笔记本")
    if (title.trim() === "") {
        alert("笔记本名不能为空")
        return
    }
    Notebooks.addNotebook({title}).then(res => {
        console.log(res)
        res.data.friendlyCreatedAt = friendlyDate(res.data.createdAt)
        alert(res.msg)
        this.notebooks.unshift(res.data)  //新增后改变现有数据集
    })
},
    onEdit (notebook) {
        let title = window.prompt("修改标题", notebook.title)
        if (title.trim() === "") {
            alert("笔记本名不能为空")
            return
        }
        Notebooks.updateNotebook(notebook.id, {title}).then(res => {
            console.log(res)
            alert(res.msg)
            notebook.title = title  //新增后改变现有数据
        })
    },
        onDelete (notebook) {
            let isConfirm = window.confirm("确定要删除吗")
            if (isConfirm) {
                Notebooks.deleteNotebook(notebook.id).then(res => {
                    console.log(res)
                    alert(res.msg)
                    this.notebooks.splice(this.notebooks.indexOf(notebook), 1)
                    //删除后改变现有数据
                })
            }
        }
```

### 获取数据后对数据进行处理

```javascript
getAll() {
    return new Promise((resolve, reject) => {
      request(URL.GET)
        .then(res => {
          //根据时间排序
          res.data = res.data.sort((notebook1, notebook2) => util.dateToTime(notebook1.createdAt) < util.dateToTime(notebook2.createdAt) ? 1 : -1)
          console.log(util.friendlyDate)
          console.log(res.data)
          //转换时间
          res.data.forEach(notebook => {
            notebook.friendlyCreatedAt = util.friendlyDate(notebook.createdAt)
          })
          resolve(res)
        }).catch(err => {
          reject(err)
        })
    })
  },
```

### 关闭eslint

*PS: 关闭eslint，config-index---useEslint false*

### 图片懒加载 lazy

https://juejin.im/post/59a7725b6fb9a02497170459

https://blog.csdn.net/a5252145/article/details/85230485

```javascript
<img v-lazy="item.path" alt="图片不存在" />
image[lazy="loading"] {
  width: 40px;
  height: 300px;
  margin: auto;
}
```

### 图片预览 vue-preview

https://www.npmjs.com/package/vue-preview

```javascript
//main
npm i vue-preview -S
import VuePreview from 'vue-preview'
Vue.use(VuePreview)

<vue-preview :slides="photos"></vue-preview>
this.photos.forEach(element => {
  src:
  alt:
  element.msrc = element.src
  element.w = 200
  element.h = 200
})
```

### 购物车

**购物车动画**

```javascript
<mt-button type="danger" size="normal" @click="showBall">加入购物车</mt-button>
<transition name="ball" @after-enter="afterEnter">
    <div class="ball" v-if="isExit"></div>
</transition>

methods: {
    showBall () {
      this.isExit = true
    },
    afterEnter () {
      this.isExit = false
      GoodsTools.add({
        id: this.detail.id,
        num: this.num
      })
    }
}

.ball-enter-active {
  animation: bounce-in 1.5s;
}
.ball-leave {
  opacity: 0;
}
@keyframes bounce-in {
  0% {
    transform: translate3d(0, 0, 0);
  }
  50% {
    transform: translate3d(100px, -100px, 0);
  }
  75% {
    transform: translate3d(160px, 0px, 0);
  }
  100% {
    transform: translate3d(100px, 170px, 0);
  }
}
.ball {
  border-radius: 50%;
  background-color: red;
  width: 24px;
  height: 24px;
  position: absolute;
  top: 440px;
  left: 120px;
  display: inline-block;
  z-index: 9999;
}
```

**购物车存储**

```javascript
import EventBus from './EventBus'
let obj = {}
obj.getGoodsList = function () {
  return JSON.parse(window.localStorage.getItem('goods') || '{}')
}
obj.saveGoods = function (goodsList) {
  window.localStorage.setItem('goods', JSON.stringify(goodsList))
  EventBus.$emit('addShopCart', obj.getTotalCount())
}
obj.add = function (goods) {
  let goodsList = this.getGoodsList()
  goodsList[goods.id] = goods.num
  this.saveGoods(goodsList)
}
obj.getTotalCount = function () {
  let goodsList = this.getGoodsList()
  let values = Object.values(goodsList)
  let sum = 0
  values.forEach(val => { sum += val })
  return sum
}
export default obj
```

**购物车**

```javascript
<template>
  <div>
    <div v-for="(good, index) in list" :key="index" class="section">
      <div>
        <img :src="good.src" alt="" class="imgDiv" />
      </div>
      <div class="contentDiv">
        <p class="titleDiv">{{ good.title | convertStr(20) }}</p>
        <p class="bottomDiv">￥{{ good.price }}</p>
      </div>
      <p class="pNum">
        <span @click="subStract(good)">-</span><span>{{ good.num }}</span
        ><span @click="add(good)">+</span><br />
        <a href="javascript:;" @click="del(index)">删除</a>
      </p>
    </div>
    <footer>
      <p>
        购买商品数量：{{ payment.count }},&nbsp;总价格：￥{{ payment.price }}
      </p>
    </footer>
  </div>
</template>
<script>
import GoodsTools from '@/GoodsTools'
export default {
  data () {
    return {
      list: []
    }
  },
  beforeRouteLeave (to, from, next) {
    console.log(1111)
    if (confirm('do you leave?')) {
      let obj = {}
      this.list.forEach(cur => {
        obj[cur.id] = cur.num
      })
      GoodsTools.saveGoods(obj)
      next()
    } else {
      next(false)
    }
  },
  methods: {
    subStract (good) {
      good.num--
    },
    add (good) {
      good.num++
    },
    del (index) {
      this.list.splice(index, 1)
    }
  },
  computed: {
    payment () {
      let price = 0
      let count = 0
      this.list.forEach(good => {
        count += good.num
        price += good.price * good.num
      })
      return {
        count,
        price
      }
    }
  },
  created () {
    let goodsList = GoodsTools.getGoodsList()
    let ids = Object.keys(goodsList).join(',')
    this.$axios.get('getShopCartList/' + ids)
      .then(res => {
        this.list = res.data
        this.list.forEach(element => {
          if (goodsList[element.id]) {
            // element.num = goodsList[element.id]
            this.$set(element, 'num', goodsList[element.id])
          }
        })
      })
      .catch(err => console.log(err))
  }
}
</script>
<style scoped>
.section {
  display: flex;
  height: 100px;
  border-bottom: 1px solid #ddd;
  padding-top: 10px;
  padding-bottom: 10px;
}
.imgDiv {
  flex: 0 1 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 10px;
}
img {
  width: 70px;
  height: 70px;
}
.pNum span {
  border: 1px solid #333;
  padding-left: 6px;
  padding-right: 6px;
}
.pNum {
  padding-top: 30px;
  padding-left: 5px;
  padding-right: 5px;
  width: 100px;
}
.pNum span {
  margin-right: 2px;
}
.bottomDiv {
  color: red;
}
</style>
```

```javascript
//app
EventBus.$on('addShopCart', data => {
  this.num = data
})
<mt-badge type="error" size="small">{{num}}</mt-badge>
```

### 页面跳转动画

```javascript
<transition name='rv' mode="out-in">
    <router-view v-on:getChildTitle=showTitle class="tmpl"></router-view>
</transition>
.rv-enter-active, .rv-leave-active{
  transition: opacity .5s;
}
.rv-enter,.rv-leave-to{
  opacity: 0;
}
```

### 路由懒加载

```javascript
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      alias: '/notebooks',
      component: () => import('@/components/NotebookList.vue')
    },
    {
      path: '/login',
      component: () => import('@/components/Login.vue')
    },
    {
      path: '/note/:noteId',
      component: () => import('@/components/NoteDetail.vue')
    },
    {
      path: '/trash',
      component: () => import('@/components/TrashDetail.vue')
    }
  ]
})
```

### webpack优化

https://segmentfault.com/q/1010000008832754

https://webpack.docschina.org/guides/lazy-loading/

### 预渲染

### 骨架屏

### 首次加载慢

![](http://jtc-img.oss-cn-shenzhen.aliyuncs.com/19-1-5/44792853.jpg)

### 特殊函数

![](http://jtc-img.oss-cn-shenzhen.aliyuncs.com/18-12-27/74392162.jpg)



### browerlist

https://www.npmjs.com/package/browserslist

package.json

```javascript
  "browserslist": [
    "> 1%",
    "last 2 versions",
    "not ie <= 8"
  ],
```

### todolist

```javascript
<template>
  <div id="app">
    <input type="text" v-model="todo" @keydown="doAdd($event)" />
    <button @click="doAddByBtn">增加</button>
    <br />
    <h2>进行中</h2>
    <ul>
      <li v-for="(item, key) in todoing" :key="key">
        <input type="checkbox" v-model="item.checked" />
        {{ item.title }}------<button @click="doDelete(key)">
          删除
        </button>
      </li>
    </ul>

    <br />
    <h2>已完成</h2>
    <ul>
      <li v-for="(item, key) in todoed" :key="key">
        <input type="checkbox" v-model="item.checked" />
        {{ item.title }}------<button @click="doDelete(key)">
          删除
        </button>
      </li>
    </ul>
  </div>
</template>

<script>
import storage from './model/storage'

export default {
  name: 'app',
  data () {
    return {
      todo: '',
      list: []
    }
  },
  methods: {
    doAdd (e) {
      console.log(e)
      if (e.keyCode !== 13) {
        return;
      }
      this.list.push({
        title: this.todo,
        checked: false
      })
      this.todo = ''
    },
    doAddByBtn () {
      this.list.push({
        title: this.todo,
        checked: false
      })
      this.todo = ''
    },
    doDelete (key) {
      this.list.splice(key, 1)
    }
  },
  mounted () {
    let curList = storage.get('list')
    if (curList) {
      this.list = curList
    }
  },
  /**监控数据变化 更新缓存 */
  watch: {
    list: {//深度监听，可监听到对象、数组的变化
      handler (val, oldVal) {
        storage.set('list', this.list)
      },
      deep: true
    }
  },
  /**v-for v-if不能同时使用，所以使用计算属性来获取数据 */
  computed: {
    todoing () {
      return this.list.filter(item => !item.checked)
    },
    todoed () {
      return this.list.filter(item => item.checked)
    }
  }
}
</script>

<style lang="scss">
</style>

```

### elementui eltable 超出自动隐藏 tooltip

 https://segmentfault.com/q/1010000014215973/a-1020000014218203 

### Failed to resolve filter

 https://segmentfault.com/q/1010000018135585 

### vue 实现分转元的 过滤器

 https://www.bbsmax.com/A/n2d9Drm05D/ 

**elementUI checkbox-group 多选自定义传参**

 https://blog.csdn.net/p930318/article/details/87690957 

### Vue + element UI 项目中el-input只能输入正整数的验证

 https://blog.csdn.net/weixin_40137911/article/details/84754029 

### vue接收后台文件流

 https://blog.csdn.net/u010598111/article/details/85052865 

### 按需导入ant design vue

 https://zhuanlan.zhihu.com/p/60869813 

### vscode自动引入模块
 https://segmentfault.com/a/1190000015690038 

### vueclie3设置devserver超时时间

 https://blog.csdn.net/qq_36727756/article/details/93891412 

### 首屏优化

 https://mp.weixin.qq.com/s/u62PhW0_pVcQdOtdr_3t3w 

### vue优化

 https://www.toutiao.com/a6739867316831388163/?tt_from=android_share&utm_campaign=client_share&timestamp=1569599431&app=news_article&utm_medium=toutiao_android&req_id=201909272350300100140470731BDC128C&group_id=6739867316831388163 

### 优化ant

 https://juejin.im/post/5ce750ce518825335847f6ad 