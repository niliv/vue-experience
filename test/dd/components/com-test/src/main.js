import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import MintUI from 'mint-ui';
import 'mint-ui/lib/style.css';
import Vue from "vue";
import VueResource from "vue-resource";
import App from "./App.vue";
import "./assets/basic.scss";
import router from './router/router';

Vue.use(ElementUI);
Vue.use(VueResource);
Vue.use(MintUI);


new Vue({
  el: "#app",
  router,
  render: h => h(App)
});
