import Vue from "vue";
import VueRouter from "vue-router";
import Content from "../components/Content";
import Home from "../components/Home";
import News from "../components/News";
import User from '../components/User';
//使用插件
Vue.use(VueRouter)
//定义路由规则
const routes = [{
    path: "/home",
    component: Home
  },
  {
    path: "/news",
    component: News,
    name: 'news'
  },
  {
    path: "/content/:id",
    component: Content
  },
  {
    path: "/user",
    component: User
  },
  {
    path: "*", //没有匹配到
    redirect: "/home"
  } /*默认跳转路由*/
];
//定义路由对象
const router = new VueRouter({
  mode: 'history',
  routes // （缩写）相当于 routes: routes
});
//导出路由
export default router;
