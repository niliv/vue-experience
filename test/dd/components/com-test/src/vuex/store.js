import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

var state = {
  count: 1
}

var mutations = {
  incCount() {
    ++state.count
  }
}

var getters = {
  computedCount: (state) => {
    return state.count * 2;
  }
}

var actions = {
  incMutationsCount(context) {
    context.commit('incCount')
  }
}

var store = new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})

export default store;
