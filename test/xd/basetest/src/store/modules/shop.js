const state = {
  shop: {}
}

const getters = {
  shop: () => JSON.parse(sessionStorage.getItem('shop'))
}

const mutations = {
  setShop(state, payload) {
    sessionStorage.setItem('shop', JSON.stringify(payload))
    state.shop = payload
  }
}

const actions = {
  setShop({ commit }, payload) {
    commit('setShop', payload)
  }
}

export default {
  getters,
  state,
  mutations,
  actions
}
