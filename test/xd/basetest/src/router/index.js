import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/table',
    name: 'Table',
    component: () => import('../views/table.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
