import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/main',
    name: 'main',
    component: () => import('@/views/main')
  }
]

const router = new VueRouter({
  routes
})

export default router
